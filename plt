#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import sys, os, h5py


def plot1d(ax,filename,prm):
    r        = np.array(h5py.File(filename, 'r')['/mesh/points/x'])
    r        = 0.5 * (r[ :-1] + r[1 : ])
    primitive = []
    # for file in filename:
    ax.plot(r, np.array(h5py.File(filename,'r')['/primitive/'+prm]),label=filename)

    ax.set_xlabel(r'$r$')
    ax.set_ylabel(prm)
    ax.set_title(prm)
    ax.legend()

def plot1dequator(ax, filename,prm):
    print(filename)
    r        = np.array(h5py.File(filename, 'r')['/mesh/points/x'])
    theta    = np.array(h5py.File(filename, 'r')['/mesh/points/y'])

    r        = 0.5 * (r[ :-1] + r[1 : ])
    equator  = len(theta) / 2 - 1

    primitive = []
    # for file in filename:
    ax.plot(r, np.array(h5py.File(filename,'r')['/primitive/'+prm])[:, equator],label = filename)

    ax.set_xlabel(r'$r$')
    ax.set_ylabel(prm)
    ax.set_title(prm)
    ax.legend()


def plot2d(ax, filename, prm):
    file = h5py.File(filename,'r')

    simulationtime = file['/status/simulationTime'].value
    primitive  = np.array(file['/primitive/' + prm]).T
    soundspeed = 5. / 3 * np.array(file['/primitive/pressure']).T / np.array(file['/primitive/density']).T
    avgsoundspeed = (np.max(soundspeed) + np.min(soundspeed)) * 0.5
    t_sc          = file['user/r1'].value / avgsoundspeed

    r = np.log10(np.array(file['/mesh/points/x']))
    theta = np.array(file['/mesh/points/y'])

    primitive_avg = np.mean(primitive.T, axis=1).reshape(len(primitive.T),1)
    primitive_avg_matrix = np.tile(primitive_avg, len(theta) - 1)

    dprimitive = (primitive.T - primitive_avg_matrix) / primitive_avg_matrix

    r_grid, theta_grid = np.meshgrid(r, theta)
    x,y = r_grid*np.cos(theta_grid), r_grid*np.sin(theta_grid)

    if (prm == 'velocity1' or prm == 'velocity2'):
        cax = ax.pcolormesh(x, y, primitive / soundspeed)
        clbr = fig.colorbar(cax)

        if (prm == 'velocity1'):
            ax.set_title('Radial velocity in units of Mach number'+ r' ($c_s$), t='+str(np.around(simulationtime / t_sc ,2)), fontsize=15)
            clbr.ax.set_title(r'$c_s$')
        if (prm == 'velocity2'):
            plt.title('Azimuthal velocity in units of Mach number'+ r' ($c_s$), t='+str(np.around(simulationtime / t_sc,2)), fontsize=15)
            clbr.ax.set_title(r'$c_s$')

    else:
        cax = ax.pcolormesh(x, y, primitive)
        clbr = fig.colorbar(cax)
        clbr.ax.set_title(r'$\frac{\Delta \rho}{\rho}$')
        ax.set_title('Fractional ' + prm + ' changes relative to background: '+ r'$\frac{ \rho - \bar{\rho}}{\bar{\rho}}$, t='+str(np.around(simulationtime / t_sc, 2)) + r'$t_{sc}$', fontsize=15 )

    ax.set_xlabel(r'$\log_{10}(r) \cos(\theta)$', fontsize=15)
    ax.set_ylabel(r'$\log_{10}(r) \sin(\theta)$', fontsize=15)


if (sys.argv[1] == '-1d'):
    fig, ax = plt.subplots()
    filename = sys.argv[3:]
    prm      = sys.argv[2]

    plot1d(ax,filename,prm)
    plt.show()

if (sys.argv[1] == '--equator'):
    fig, ax = plt.subplots()
    filename = sys.argv[3:]
    prm      = sys.argv[2]

    plot1dequator(ax,filename,prm)
    plt.show()

if (sys.argv[1] == '-2d'):
    #generate array of primitive data
    fig = plt.figure(figsize=(10,10))
    ax = plt.subplot(111)
    prm = sys.argv[2]
    filename = sys.argv[3]

    plot2d(ax, filename, prm)
    plt.show()

if (sys.argv[1] == '--soundspeed'):
    def sound():
        file = h5py.File('chkpt.0000.h5','r')
        r = np.array(file['mesh/points/x'])
        q = np.array(file['mesh/points/y'])
        gm = file['user/gamma']
        g0 = file['user/g0']
        d = np.array(file['primitive/density'])[:,q.shape[0] / 2]
        p = np.array(file['primitive/pressure'])[:, q.shape[0] / 2]

        r = 0.5 * (r[1:] + r[:-1])

        escape = np.sqrt(g0 / r)
        sound = np.sqrt(gm * p / d)

        plt.loglog(r, escape, ls='--', label='escape velocity')
        plt.loglog(r,d, label = 'density')
        plt.loglog(r,p, label = 'pressure')

        plt.plot(r,sound, label = 'sound speed')


        plt.legend()
        plt.show()

    sound()

if (sys.argv[1] == '-dqdr'):
    def dqdr():
        file  = h5py.File('chkpt.0000.h5','r')
        g0    = 0.25 #GM
        K     = 1.0
        gamma = 5.0 / 3.0
        alpha = (gamma - 1.0) / gamma
        beta  = g0 / K * alpha
        rho0  = 1.0
        v0    = rho0**(gamma - 1)
        r    = np.linspace(1,1000,1000)

        density  = (v0 * r**(-2.0 * alpha) - beta / (2.0* alpha - 1.0) * (1 / r))**(1 / (gamma - 1.0))
        pressure = K * density ** gamma
        energy   = pressure / (gamma - 1.0) / density
        escape   = np.sqrt(g0/r)
        dQdr = 4.0 * np.pi * r**2 * density**2 * energy**-0.5

        plt.loglog(r, density, label = 'density')
        plt.loglog(r, pressure, label = 'pressure')
        plt.loglog(r, np.sqrt(gamma*pressure/density), label ='soundspeed')
        plt.loglog(r, escape, label = 'escape velocity')
        plt.loglog(r, escape/ np.sqrt(gamma*pressure/density), label= 'escape / sound speed')
        plt.loglog(r, dQdr, label=r'$\frac{d\dot{Q}}{dr}$', ls='--')

        plt.legend()
        plt.xlabel(r'$\log(r)$', fontsize=20)
        plt.ylabel(r'$\log(\rho), \log(P), \log(c_s), \log(\frac{d\dot{Q}}{dr})$', fontsize=20)
        plt.show()

    dqdr()

if (sys.argv[1] == '--movie'):
    from matplotlib.animation import FuncAnimation
    import matplotlib.animation as animation
    import sys, os, h5py, re, math

    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=50, bitrate=50000)

    #generate array of primitive data
    filename = os.listdir(".")
    filename.sort()
    filename = filename[ :-1]

    file  = h5py.File(filename[0],'r')
    r     = np.log10(np.array(file['/mesh/points/x']))
    theta = np.array(file['/mesh/points/y'])

    time          = []
    primitive     = []
    primitive_avg = []
    for fil in filename:
        density = np.array(h5py.File(fil, 'r')['/primitive/density'])
        time.append(h5py.File(fil,'r')['/status/simulationTime'].value)
        primitive.append(density.T)
        primitive_avg.append(density.T - (np.tile(np.mean(density, axis = 1).reshape(len(density), 1), len(theta)-1)).T)
    time = np.around(time,decimals=2)

    r_grid, theta_grid = np.meshgrid(r, theta)
    x, y = r_grid * np.cos(theta_grid), r_grid * np.sin(theta_grid)

    fig = plt.figure()
    ax  = plt.subplot(111)

    cax = ax.pcolormesh(x, y, primitive_avg[0], vmin=-0.25, vmax=0.25)
    fig.colorbar(cax)
    ax.set_title("Time: "+ str(time[0]))

    def animate(i):
        cax.set_array(primitive_avg[i].flatten())
        cax.set_clim(0.5*np.min(primitive_avg[i]),0.8*np.max(primitive_avg[i]))
        ax.set_title("Time: "+ str(time[i]))

    anim = FuncAnimation(fig, animate, interval=1, frames=1600, repeat=False, save_count=1600) #set frame here

    plt.draw()
    anim.save('density.mp4', writer=writer)
    plt.show()

if (sys.argv[1] == '--twelve'):
    foldernames = ['heatf01/','heatf05/','heatf10/','heatf50/','heatf100/']
    filenames = ['chkpt.0000.h5','chkpt.0120.h5','chkpt.0240.h5','chkpt.0360.h5','chkpt.0480.h5','chkpt.0600.h5','chkpt.0720.h5','chkpt.0840.h5','chkpt.0960.h5','chkpt.1080.h5','chkpt.1200.h5','chkpt.1320.h5']
    fig, ax = plt.subplots(len(foldernames),len(filenames), sharey=True, sharex= True, figsize=(60,50))
    for folderindex in range(0,len(foldernames)):
        for fileindex in range(0,len(filenames)):
            file = h5py.File(foldernames[folderindex] + filenames[fileindex],'r')

            simulationtime = file['/status/simulationTime'].value
            primitive  = np.array(file['/primitive/' + 'density']).T
            soundspeed = 5. / 3 * np.array(file['/primitive/pressure']).T / np.array(file['/primitive/density']).T
            avgsoundspeed = (np.max(soundspeed) + np.min(soundspeed)) * 0.5
            t_sc          = file['user/r1'].value / avgsoundspeed

            r = np.log10(np.array(file['/mesh/points/x']))
            theta = np.array(file['/mesh/points/y'])

            primitive_avg = np.mean(primitive.T, axis=1).reshape(len(primitive.T),1)
            primitive_avg_matrix = np.tile(primitive_avg, len(theta) - 1)

            dprimitive = (primitive.T - primitive_avg_matrix) / primitive_avg_matrix

            r_grid, theta_grid = np.meshgrid(r, theta)
            x,y = r_grid*np.cos(theta_grid), r_grid*np.sin(theta_grid)

            # cax.append(ax[folderindex , fileindex].pcolormesh(x, y, dprimitive.T))
            if (fileindex == 0):
                ax[folderindex , fileindex].pcolormesh(x, y, dprimitive.T, vmin = -0.25, vmax=0.25)
                clbr = fig.colorbar(ax[folderindex , fileindex].pcolormesh(x, y, dprimitive.T, vmin = -0.25, vmax=0.25), ax=ax[folderindex , fileindex])
            else:
                ax[folderindex , fileindex].pcolormesh(x, y, dprimitive.T)
                clbr = fig.colorbar(ax[folderindex , fileindex].pcolormesh(x, y, dprimitive.T), ax=ax[folderindex , fileindex])
            clbr.ax.set_title(r'$\frac{\Delta \rho}{\rho}$')
            ax[folderindex, fileindex].set_title( r'$t=$'+str(np.around(simulationtime / t_sc, 2)) + r'$t_{sc}$', fontsize=15 )

    ax[0,0].set_ylabel(r'$f=0.01$', fontsize=15)
    ax[1,0].set_ylabel(r'$f=0.05$', fontsize=15)
    ax[2,0].set_ylabel(r'$f=0.10$', fontsize=15)
    ax[3,0].set_ylabel(r'$f=0.50$', fontsize=15)
    ax[4,0].set_ylabel(r'$f=1.00$', fontsize=15)

    fig.text(0.5, 0.08, r'$\log_{10}(r) \cos(\theta)$', ha='center', va='center', fontsize=20)
    fig.text(0.09, 0.5, r'$\log_{10}(r) \sin(\theta)$', ha='center', va='center', rotation='vertical', fontsize=20)
    plt.savefig('time_evolution.png')
    plt.show()

if (sys.argv[1] == '--debug'):
    fig, ax = plt.subplots()
    prm      = sys.argv[2]
    filename1 = sys.argv[3]
    filename2 = sys.argv[4]

    plot1d(ax,filename1,prm)
    plot1dequator(ax, filename2,prm)
    plt.show()
