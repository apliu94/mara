#include <cmath>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include "ThermalConvection.hpp"
#include "../Mara.hpp"
#include "../BoundaryConditions.hpp"
#include "../BlockDecomposition.hpp"
#include "../CartesianMeshGeometry.hpp"
#include "../SphericalMeshGeometry.hpp" //include spherical mesh geometry
#include "../CellCenteredFieldCT.hpp"
#include "../Checkpoint.hpp"
#include "../ConservationLaws.hpp"
#include "../FieldOperator.hpp"
#include "../IntercellFluxSchemes.hpp"
#include "../MeshData.hpp"
#include "../MeshOperator.hpp"
#include "../Problems.hpp"
#include "../RiemannSolvers.hpp"
#include "../SolutionSchemes.hpp"
#include "../TaskScheduler.hpp"
#include "../TimeSeriesManager.hpp"
#include "MPI.hpp"
#include "HDF5.hpp"
#include "Timer.hpp"
#include "Logger.hpp"
#define cot(x) std::tan (M_PI_2 - x)
using namespace Cow;




/**
Boundary conditions for a vertical atmosphere. The x and y directions are
periodic. The floor is reflecting and the ceiling is currently set to the
value of the initial condition.
*/
class ThermalConvectionBoundaryCondition : public BoundaryCondition
{
public:
    ThermalConvectionBoundaryCondition()
    {
        reflecting.setConservationLaw (std::make_shared<NewtonianHydro>());
    }

    void setMeshGeometry (std::shared_ptr<MeshGeometry> geometryToUse) override
   {
       geometry = geometryToUse;
   }

/** Manual relfecting boundary condition class  
   void apply (
       Cow::Array& A, //boundary condition array
       MeshLocation location, //enum class {vert, edge, face, cell} in mara.hpp
       MeshBoundary boundary, //either left or right, enum class
       int axis, //0 ,1 or 2
       int numGuard) const override //number of guardzones; overrides virtual method in BoundaryConditions class in Mara.hpp
   {
       switch (axis) // this will be iterated in the applySimple method in BoundaryConditions.cpp, which is called in MeshData as applyBoundaryCondition
       {
           case 0:
           if (boundary == MeshBoundary::right)
           {
              for (int i = A.size(0) - numGuard; i < A.size(0); ++i) //looping over radial guardzones
              {
                 for (int j = 0; j < A.size(1); ++j)
                 {
                     for (int k = 0; k < A.size(2); ++k)
                     {
                         for (int q = 0; q < 5; ++q)
                         {
                            if (q == 1 && i == A.size(0) - numGuard)
                            {
                                A(i, j, k, q) = -1.0 * A(i - numGuard + 1, j, k, q);
                            }
                            if (q == 1 && i == A.size(0) - numGuard + 1)
                            {
                                A(i, j, k, q) = -1.0 * A(i - numGuard - 1, j, k, q);
                            }
                            if (q != 1 && i == A.size(0) - numGuard)
                            {
                                A(i, j, k, q) = A(i - numGuard + 1, j, k, q);
                            }
                            if (q != 1 && i == A.size(0) - numGuard + 1)
                            {
                                A(i, j, k, q) = A(i - numGuard - 1, j, k, q);
                            }
                         }
                     }
                 }
             }
          }

          if (boundary == MeshBoundary::left)
          {
             for (int i = 0; i < numGuard; ++i) //looping over radial guardzones
             {
                for (int j = 0; j < A.size(1); ++j)
                {
                    for (int k = 0; k < A.size(2); ++k)
                    {
                        for (int q = 0; q < 5; ++q)
                        {
                           if (q == 1 && i == 0)
                           {
                               A(i, j, k, q) = -1.0 * A(i + numGuard + 1, j, k, q);
                           }
                           if (q == 1 && i == 1)
                           {
                               A(i, j, k, q) = -1.0 * A(i + numGuard - 1, j, k, q);
                           }
                           if (q != 1 && i == 0)
                           {
                               A(i, j, k, q) = A(i + numGuard + 1, j, k, q);
                           }
                           if (q != 1 && i == 1)
                           {
                               A(i, j, k, q) = A(i + numGuard - 1, j, k, q);
                           }
                        }
                    }
                }
              }
           }

           case 1:
           if (boundary == MeshBoundary::right)
           {
              for (int i = 0; i < A.size(0); ++i) //looping over radial guardzones
              {
                 for (int j =  A.size(1) - numGuard; j < A.size(1); ++j)
                 {
                     for (int k = 0; k < A.size(2); ++k)
                     {
                         for (int q = 0; q < 5; ++q)
                         {
                            if (q == 2 && j == A.size(1) - numGuard)
                            {
                                A(i, j, k, q) = -1.0 * A(i, j - numGuard + 1, k, q);
                            }
                            if (q == 2 && j == A.size(1) - numGuard + 1)
                            {
                                A(i, j, k, q) = -1.0 * A(i, j - numGuard - 1, k, q);
                            }
                            if (q != 2 && j == A.size(1) - numGuard)
                            {
                                A(i, j, k, q) = A(i, j - numGuard + 1, k, q);
                            }
                            if (q != 2 && j == A.size(1) - numGuard + 1)
                            {
                                A(i, j, k, q) = A(i, j - numGuard - 1, k, q);
                            }
                         }
                     }
                 }
             }
           }

           if (boundary == MeshBoundary::left)
           {
             for (int i = 0; i < A.size(0); ++i) //looping over radial guardzones
             {
                for (int j = 0; j < numGuard; ++j)
                {
                    for (int k = 0; k < A.size(2); ++k)
                    {
                        for (int q = 0; q < 5; ++q)
                        {
                           if (q == 2 && j == 0)
                           {
                               A(i, j, k, q) = -1.0 * A(i, j + numGuard + 1, k, q);
                           }
                           if (q == 2 && j == 1)
                           {
                               A(i, j, k, q) = -1.0 * A(i, j + numGuard - 1, k, q);
                           }
                           if (q != 2 && j == 0)
                           {
                               A(i, j, k, q) = A(i, j + numGuard + 1, k, q);
                           }
                           if (q != 2 && j == 1)
                           {
                               A(i, j, k, q) = A(i, j + numGuard - 1, k, q);
                           }
                        }
                    }
                }
            }
           }

        }
    }
*/
    void apply (Cow::Array& A, MeshLocation location, MeshBoundary boundary, int axis, int numGuard) const override
    {
        switch (axis)
        {
            case 0:
            {
                if (boundary == MeshBoundary::left)
                {
                    return reflecting.apply (A, location, boundary, axis, numGuard);
                }
                if (boundary == MeshBoundary::right)
                {
                    return reflecting.apply (A, location, boundary, axis, numGuard);
                }
            }
            case 1: return periodic.apply (A, location, boundary, axis, numGuard);
            default: throw std::logic_error ("ThermalConvectionBoundaryCondition: not yet configured for 3D");
        }
    }

    bool isAxisPeriodic (int axis) override
    {
        switch (axis)
        {
            case 0: return false;
            case 1: return true;
            case 2: return true;
            default: throw std::logic_error ("ThermalConvectionBoundaryCondition");
        }
    }

    ReflectingBoundaryCondition reflecting;
    PeriodicBoundaryCondition periodic;
    OutflowBoundaryCondition outflow;
    std::shared_ptr<MeshGeometry> geometry;
};




/**
Boundary conditions for a vertical atmosphere with sound-wave driving at the
inner boundary.
*/
class AcousticDriverBoundaryCondition : public BoundaryCondition
{
public:
    AcousticDriverBoundaryCondition()
    {
        reflecting.setConservationLaw (std::make_shared<NewtonianHydro>()   );
    }

    void setSimulationTime (double newSimulationTime) override
    {
        simulationTime = newSimulationTime;
    }

    void setBoundaryValueFunction (InitialDataFunction initialDataFunctionToUse) override
    {
        initialDataFunction = initialDataFunctionToUse;
    }

    void setMeshGeometry (std::shared_ptr<MeshGeometry> mg) override
    {
        meshGeometry = mg;
    }

    void apply (Cow::Array& A, MeshLocation location, MeshBoundary boundary, int axis, int numGuard) const override
    {
        if (axis != 0)
        {
            throw std::logic_error ("AcousticDriverBoundaryCondition: only configured for 1D");
        }
        if (boundary == MeshBoundary::left)
        {
            reflecting.apply (A, location, boundary, axis, numGuard);
            return applyInflowAtInnerBoundary (A, numGuard);
        }
        return outflow.apply (A, location, boundary, axis, numGuard);
    }

    void applyInflowAtInnerBoundary (Cow::Array& P, int numGuard) const
    {
        double omega = 2.0;
        double mach = 0.01;

        for (int i = 0; i < numGuard; ++i)
        {
            auto r = meshGeometry->coordinateAtIndex (i - numGuard, 0, 0)[0];

            auto P0 = initialDataFunction (r, 0, 0);
            auto u0 = 0.0;
            auto d0 = P[0];
            auto p0 = P[4];
            auto cs = std::sqrt (5. / 3 * p0 / d0);

            auto u1 = cs * mach;
            //auto d1 = d0 * mach;
            //auto p1 = d0 * cs * cs;

            //P(i, 0, 0, 0) = d0 + d1 * std::sin (omega * simulationTime);
            P(i, 0, 0, 1) = u0 + u1 * std::sin (omega * simulationTime);
            //P(i, 0, 0, 2) = 0.0;
            //P(i, 0, 0, 3) = 0.0;
            //P(i, 0, 0, 4) = p0 + p1 * std::sin (omega * simulationTime);
        }
    }

    bool isAxisPeriodic (int axis) override
    {
        switch (axis)
        {
            case 0: return false;
            case 1: return false;
            case 2: return true;
            default: throw std::logic_error ("AcousticDriverBoundaryCondition");
        }
    }

    double simulationTime = 0.0;
    OutflowBoundaryCondition outflow;
    ReflectingBoundaryCondition reflecting;
    InitialDataFunction initialDataFunction = nullptr;
    std::shared_ptr<MeshGeometry> meshGeometry;
};




// ============================================================================
int ThermalConvectionProgram::run (int argc, const char* argv[])
{
    auto status = SimulationStatus();
    auto user = Variant::NamedValues();
    user["outdir"]  = "data";
    user["restart"] = "";
    user["tfinal"]  = 100.0;
    user["serial"]  = false;
    user["cpi"]     = 0.25;
    user["cpf"]     = "single"; // or multiple
    user["tsi"]     = 0.1;
    user["cfl"]     = 0.5;
    user["plm"]     = 2.0;
    user["Nr"]      = 64;
    user["Nt"]      = 64;
    user["r0"]      = 1.0;     // inner radius
    user["r1"]      = 10.0;  // outer radius
    user["rv"]      = 1000.0;  // virial radius of cluster: r0 * alpha / (1. - alpha)
    user["uniform"] = false;

    user["gamma"]   = 5. / 3;  // adiabatic index
    user["rho0"]    = 1.;      // inner density
    user["K"]       = 1.;      // entropy
    user["g0"]      = 0.50;     // G * M_dm was 0.25
    user["M"]       = 1.0; // total mass 0.00026
    user["f"]       = 0.01;    // net heating coefficient
    user["delta"]   = 1.0;     // atmosphere perturbation
    user["omega"]   = 1.0;     // angular frenquency of sound wave source at inner boundary
    user["A"]       = 0.25;     // amplitude of sound wave source at inner boundary


    auto clineUser = Variant::fromCommandLine (argc, argv);
    auto chkptUser = Variant::NamedValues();

    if (clineUser.find ("restart") != clineUser.end())
    {
        chkptUser = H5::File (clineUser["restart"], "r").getGroup ("user").readNamedValues();
    }
    Variant::update (user, chkptUser);
    Variant::update (user, clineUser);



    const double g0      = user["g0"];
    const double M       = user["M"];
    const double rho0    = user["rho0"];
    const double gamma   = user["gamma"];
    const double omega   = user["omega"];
    const double A       = user["A"];
    const double K       = user["K"];
    const double r0      = user["r0"];
    const double delta   = user["delta"];
    const double rv      = user["rv"];

    const double f       = user["f"];
    const double v_ff    = std::pow(g0 / r0, 0.5);
    const double t_ff    = r0 / v_ff;
    const double U0      = g0 * M / r0;
    const double L0      = std::pow(r0, 2) * (8. * M_PI * rho0) * std::pow( (rv / (r0 + rv)) * (gamma / (gamma - 1.)) * K * std::pow(rho0, gamma - 1.), 1.5);
    const double q_heat0 = f * L0 * std::pow(r0, -3.);
    // const double q_cool  = q_heat0;
    double soundSpeed   = 0.0;

    // Gravitational source terms, heating, and initial data function
    // ------------------------------------------------------------------------
    auto sourceTermsFunction = [&] (double r, double q, double p, StateArray P)
    {
        const double dg = P[0];
        const double vr = P[1];
        const double vq = P[2];
        const double vp = P[3];
        const double pg = P[4];
        auto S = StateArray();


        S[0] = 0.0;
        S[1] = (2 * pg + dg * (vq * vq + vp * vp)) / r;
        S[2] = (pg * cot(q) + dg * (vp * vp * cot(q) - vr * vq)) / r;
        S[3] = -dg * vp * (vr + vq * cot(q)) / r;
        S[4] = 0.0;


        const double g = g0 * std::pow (r, -2.0);
        S[1] += -dg * g;
        S[4] += -dg * g * vr;

        // if (soundSpeed < 5.0)
        // {
            // S[4] += q_heat0 * std::exp (-1. * r / r0);
        // }

        //Bremsstrahlung cooling
        // S[4] -= 0.01 * q_cool * std::pow(dg, 2.) * std::pow (pg / (gamma - 1.) / dg, -0.5);
        // A and B are have density and internal energy dimensions to normalize the variables

        return S;
    };

    auto initialData = [&user, g0, r0, rho0, gamma, K, delta] (double r, double q, double p) -> std::vector<double>
    {
        using std::pow;

        // double drho_rho = delta * 1e-3 * rand() / RAND_MAX;

        if (! user["uniform"])
        {
            auto d0  = rho0;
            auto a   = (gamma - 1.) / gamma / K * g0 / r0 / pow(d0, gamma - 1.);
            auto d   = d0 * pow((1. - a * (1. - r0 / r)), 1. / (gamma - 1.));
            auto pre = K * pow(d, gamma);
            // return {d * (1 + drho_rho), 0, 0, 0, pre};
            return {d, 0, 0, 0, pre};
        }
        else
        {
            const double rho = 1.0;
            const double pre = 1.0;
            return std::vector<double> {rho, 0, 0, 0, pre};
        }
    };

    auto logger    = std::make_shared<Logger>();
    auto writer    = std::make_shared<CheckpointWriter>();
    auto tseries   = std::make_shared<TimeSeriesManager>();
    auto scheduler = std::make_shared<TaskScheduler>();
    double timestepSize = 0.0;

    auto bd = std::shared_ptr<BlockDecomposition>();
    auto bc = std::shared_ptr<BoundaryCondition> (new ThermalConvectionBoundaryCondition);
    // auto bc = std::shared_ptr<BoundaryCondition> (new AcousticDriverBoundaryCondition);
    auto rs = std::shared_ptr<RiemannSolver> (new HllcNewtonianHydroRiemannSolver);
    auto fs = std::shared_ptr<IntercellFluxScheme> (new MethodOfLinesPlm);
    auto cl = std::make_shared<NewtonianHydro>();


    // Set up grid shape. In 1D it's z-only. In 2D it's the x-z plane.
    // ------------------------------------------------------------------------
    auto mg = std::shared_ptr<MeshGeometry> (new SphericalMeshGeometry);
    auto bs = Shape {{ 2, int (user["Nt"]) == 1 ? 0 : 2, 0, 0, 0 }};
    mg->setCellsShape ({{ user["Nr"], user["Nt"], 1 }});
    mg->setLowerUpper ({{ user["r0"], M_PI * 0.5 - M_PI / 2.0, 0}}, {{user["r1"], M_PI * 0.5 + M_PI / 2.0 , 0.1}}); //M_PI * 0.5 - M_PI / 12.0

    if (! user["serial"])
    {
        logger->setLogToNullUnless (MpiCommunicator::world().isThisMaster());
        bd = std::make_shared<BlockDecomposition> (mg, *logger);
        mg = bd->decompose();
        bc = bd->createBoundaryCondition (bc);
    }

    tseries->setLogger (logger);
    scheduler->setLogger (logger);

    auto ss = std::make_shared<MethodOfLinesTVD>();
    auto mo = std::make_shared<MeshOperator>();
    auto fo = std::make_shared<FieldOperator>();
    auto md = std::make_shared<MeshData> (mg->cellsShape(), bs, cl->getNumConserved());

    cl->setGammaLawIndex (double (user["gamma"]));
    fs->setPlmTheta (double (user["plm"]));
    fs->setRiemannSolver (rs);
    fo->setConservationLaw (cl);
    mo->setMeshGeometry (mg);
    ss->setSourceTermsFunction (sourceTermsFunction);
    ss->setMeshOperator (mo);
    ss->setFieldOperator (fo);
    ss->setBoundaryCondition (bc);
    ss->setRungeKuttaOrder (2);
    ss->setDisableFieldCT (true);
    ss->setIntercellFluxScheme (fs);

    md->setVelocityIndex (cl->getIndexFor (ConservationLaw::VariableType::velocity));
    bc->setMeshGeometry (mg);
    bc->setConservationLaw (cl);
    bc->setBoundaryValueFunction (initialData);
    bc->setMeshGeometry (mg);

    auto L         = mo->linearCellDimension();
    auto V         = mo->measure (MeshLocation::cell);
    auto P         = md->getPrimitive();
    auto advance   = [&] (double dt) { return ss->advance (*md, dt); };
    auto condition = [&] () { return status.simulationTime < double (user["tfinal"]); };
    auto timestep  = [&] () { return timestepSize; };

    auto taskMaxSoundSpeed = [&] (SimulationStatus, int rep)
    {
        double localSoundSpeed = double (user["cfl"]) * fo->getMaxSoundSpeed (P);
        soundSpeed = MpiCommunicator::world().maximum (localSoundSpeed);
    };

    auto taskRecomputeDt = [&] (SimulationStatus, int rep)
    {
        double localDt = double (user["cfl"]) * fo->getCourantTimestep (P, L);
        timestepSize = MpiCommunicator::world().minimum (localDt);
    };

    auto taskCheckpoint = [&] (SimulationStatus, int rep)
    {
        writer->writeCheckpoint (rep, status, *cl, *md, *mg, *logger);
    };

    //lambda for computing primitives averaged over cell, rep is repetition
    auto taskTimeSeries = [&] (SimulationStatus, int rep)
    {
        //bd defined, volumeAverageOverPatches returned, skips val forloop
        auto volumeAverageOverPatches = [&] (std::vector<double> vals)
        {
            if (bd)
                return bd->volumeAverageOverPatches (vals);

            for (auto& val : vals)
                val /= mg->meshVolume();

            return vals;
        };

        auto volumeIntegrated = fo->volumeIntegratedDiagnostics (P, V);
        auto volumeAveraged = volumeAverageOverPatches (volumeIntegrated);
        auto fieldNames = cl->getDiagnosticNames();
        auto entry = Variant::NamedValues();

        for (unsigned int n = 0; n < fieldNames.size(); ++n)
        {
            entry[fieldNames[n]] = volumeAveraged[n];
        }
        tseries->append (status, entry);
    };

    auto taskSetBCSimulationTime = [bc] (SimulationStatus status, int rep)
    {
        bc->setSimulationTime (status.simulationTime);
    };

    scheduler->schedule (taskTimeSeries, TaskScheduler::Recurrence (user["tsi"]), "time_series");
    scheduler->schedule (taskCheckpoint, TaskScheduler::Recurrence (user["cpi"]), "checkpoint");
    scheduler->schedule (taskMaxSoundSpeed, TaskScheduler::Recurrence (0.0, 0.0, 1), "compute_max_cs");
    scheduler->schedule (taskRecomputeDt, TaskScheduler::Recurrence (0.0, 0.0, 16), "compute_dt");
    scheduler->schedule (taskSetBCSimulationTime, TaskScheduler::Recurrence (0.0, 0.0, 1), "set_bc_time");

    writer->setMeshDecomposition (bd);
    writer->setTimeSeriesManager (tseries);
    writer->setTaskScheduler     (scheduler);
    writer->setOutputDirectory   (user["outdir"]);
    writer->setFormat            (user["cpf"]);
    writer->setUserParameters    (user);
    writer->setFilenamePrefix    ("chkpt");

    status = SimulationStatus();
    status.totalCellsInMesh = mg->totalCellsInMesh();

    if (! user["restart"].empty())
    {
        writer->readCheckpoint (user["restart"], status, *cl, *md, *logger);
        scheduler->skipNext ("checkpoint");
    }
    else
    {
        md->assignPrimitive (mo->generate (initialData, MeshLocation::cell));
    }
    md->applyBoundaryCondition (*bc);
    taskRecomputeDt (status, 0);
    taskMaxSoundSpeed (status, 0);

    logger->log() << std::endl << user << std::endl;
    return maraMainLoop (status, timestep, condition, advance, *scheduler, *logger);
}
